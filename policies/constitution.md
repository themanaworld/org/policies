---
name: Manasource
description: Manasource Constitution
aliases: [TMWC, TMWT, MT, team, tmw_team, committee, tmw_committee, collective, tmw_collective, organization, organisation, org]

autoupdate:
  forums: {forum: 1, topic: 17166, post: 132138}
  wiki: TMW_Team
---

# Manasource
Manasource is an organisation with members from several game projects that have spawned throughout the history of the organisation.

The Mana World started as an open source 2D MMORPG in 2004 and was from the beginning constantly-evolving. Over time forks emerged of the original game and its client, as well as different server software and even plans for new titles inspired by the original game. Manasource is a collaboration between these related projects, managing shared resources like hosting, software and game content.

Our software is free and open source and we endorse the principles of free software as defined by the Free Software Foundation.

## Development Projects
- The Mana World is the designation given to the original game dating back to 2004;
- Other games related to or inspired by The Mana World;
  - Such projects administrate their own autonomous development and GM teams;
  - A member of a project's team is not necessarily a Mana Team member, unless voted so.

## Mana Team (MT, Team)
Mana Team is the executive body of Manasource. It is made up of Developers, Administrators and Game Masters with a long history within the organisation.
In order to acquire MT membership, a vote is required within the guidelines established in the membership section.

### Structure
- The purpose of MT is to make or delegate executives decisions regarding the running of Manasource.
- To become a Team member, one must be approved by a majority of MT and needs to gain membership through any official project in the one of the following categories of contributors: Developer, Game Master, Server Admin;
- Someone may hold any role within Manasource and not be a MT member, but they need to have the required knowledge or experience to fulfill that role;
- In the event that no MT members remain, but roles are fulfilled by non-MT members, those members may assume MT membership to fulfill MT's duties to the best of their ability;
- Mana Team will be the [SPI](https://spi-inc.org) Liaison or appoint one among its members.
- Regarding its development projects, MT will only act as an ultimate arbiter or in a guarantor capacity, preserving the autonomy of each project's own administration;
- MT may consider inactive a project lacking a lead or representative (can be any Admin, Developer or GM) willing to speak for it for over 90 days.

### General Duties
- Stewardship of The Mana World and all other game development projects;
- To maintain documentation and archival information relating to The Mana World and all other projects;
- To provide tutorials, training and/or time to newer contributors;
- To attempt to encourage newer contributors and to instill a culture of mutual respect and collaboration;
- To ensure the correct functioning of the official Forums and other official channels of communication (IRC, Discord, etc.).

### Issues which require a Mana Team voting poll
- Changes to MT governance and/or Organization;
- Changes to official Games, Forums and Wiki rules (not guidelines);
- Voting a member into MT, reinstating an Adviser to their former position, revoking membership, banning or suspending a member;
- Decisions on financial or legal matters affecting Manasource;
- Decisions regarding guardianship of inactive projects;
- Ultimate arbitration, if requested, in relation to an individual game project that is unable to come to a consensus;
- Formal objections to everyday decisions taken without a vote. Such objections must be raised by at least one MT member
within 30 days of any changes made that they may affect.

### How to conduct a voting poll
- A poll must be opened in the ["Mana Team" private section](https://forums.themanaworld.org/viewforum.php?f=23) of the [Forums](https://forums.themanaworld.org/);
- The poll will allow re-voting and a running time will be set for 7 to 30 days (usually 7 days);
- Once the first vote has been cast (excluding the poll author's), the poll duration may not be reduced;
- A simple majority of 50%+1 (or more, when specified) is required for a poll to have passed;
- Any valid poll must attain at least 3 votes and include an option directly against the relevant proposal or an option for status-quo.

### How to conduct a consultative poll
- A consultative poll looks much like a voting poll but is not binding and is only intended to gather opinions;
- Any poll conducted in the Mana Team private section which does not follow the format defined for a voting poll is considered a consultative poll;
- Any voting poll which does not gather a minimum of 3 votes is considered a consultative poll;
- MT members may open polls simply to gather opinions and not to take binding decisions, in this case they should clearly state in the original post that votes cast are for the purpose of opinion-gathering;
- Consultative polls may be used for governance through consensus unless objected to by a MT member.

### Consensus governance
- Consensus-based decisions are taken in the absence of any objections and in the assumption that none would be raised;
- MT may govern through consensus in the interest of efficiency;
- Further guidelines on consensus governance are outlined [on the wiki](https://wiki.themanaworld.org/index.php/Consensus);
- Consensus governance cannot be applied to constitutional changes and legal or financial decisions, which will always require a voting poll;
- Consensus may be used to act on certain issues while a voting poll is still open, but only in case it is abundantly clear that a clear opinion has already been reached;
- Any objection by any MT member will invalidate the consensus process and force a voting poll or enforce the status-quo.

### Membership
- Members are nominated by one or more Mana Team members and voted in by MT as a whole;
- To be nominated, the candidate must have been involved with an official Manasource project for at least six months, and never have been kicked from MT previously;
- Members who become inactive should be removed from MT and other associated groups, unless a justification for not doing so exists and is accepted by MT.
They should be moved to the Adviser group unless they request not to be, in which case they are to be left solely in the Contributor group;
- Inactivity is generally defined as not being present or being unavailable for a period longer than 1 year. Very low activity should be judged 
on a case-by-case basis taking individual reasons into consideration. 
- Management of memberships is tracked on the official Forums;
- Any member may call a vote to revoke someone's membership or ban a member if deemed necessary;
- At any time, any member may revoke their own membership and opt for Adviser status.

## Mana Team contributor groups
MT members roughly fit into three categories: Administrators (Admins), Developers (Devs) and Game Masters (GMs). One person may occupy one or more of these roles.

### Administrators (Admins)
- Are those that may have ssh access to the server and are responsible for server restarts, maintenance, and the general health of the  overall infrastracture (sysadmins);
- Are those who may have administrator access to essential infrastracture such as the official forum board, wiki, IRC and Discord;
  - May be tasked in this capacity to manage group memberships;
- Are usually those that deal with Account resets/info and aid in GM investigations;
- To become a member of this group, a person must demonstrate the appropriate knowledge and willingness to devote time, as well as an ability to collaborate successfully with the existing admin team;

### Developers (Devs)
- Responsible for the development of one or more official Manasource projects;
- To become a developer, a person must show appropriate knowledge, contribute to the project and gain 'git' access;
- Each individual project has its own developers who are not automatically Mana Team members, but are eligible to be nominated as such.

### Game Masters (GMs)
- Responsible for moderating and enforcing the rules of the game(s), wiki(s) and forums.
- Responsible for community management and user support.
- To become a Game Master, a person must be nominated by the player community and approved by MT though consensus or a voting poll.
- Each individual project has its own GMs who are not automatically Mana Team members, but are eligible to be nominated as such.

## Affiliated groups
These groups are closely affiliated with MT and Manasource as an organisation, but are not necessarily direct members of the Team. 
MT may suspend membership of these groups through consensus and revoke membership through voting poll.


### Advisers
- To become a member of this group, a person needs to be a prior contributor and MT member;
- MT members may be moved automatically to this group by becoming inactive for a period of 12 months or more;
  - Inactivity is defined as lack of engagement with Manasource in general or any associated projects,
   other MT members and particularly one's expected duties within MT;
  - Inactivity is judged by consensus among MT members and decided by a voting poll in case of objections;
- MT members may be moved forcefully into this group by voting poll;
- Advisers are not MT members;
  - MT members who are Sysadmins may retain ssh access upon becoming Advisers with the unanimous agreement of other Sysadmins;
- Advisers typically no longer actively contribute but may choose to do so anyway;
- Allowed to participate in internal discussions, but not allowed to vote as a MT member;
- May be reinstated into MT with a simple voting poll.

### Moderators
- MT may appoint by [Consensus](https://wiki.themanaworld.org/index.php/Consensus) Moderators to manage social media accounts, servers and channels ("communities") on behalf of Manasource;
- Each project may have its own media and appoint its own Moderators;
- Moderators are not necessarily MT members unless nominated and voted in;
- Moderators may appoint and supervise lower-ranking moderators to assist them in managing a community.

<!-- The section below only contains links to relevant user groups and is not part of the constitution -->
<br><hr>

## Members
- [Mana Team](https://forums.themanaworld.org/memberlist.php?mode=group&g=981)
  - [Administrators](https://forums.themanaworld.org/memberlist.php?mode=group&g=994)
  - [Developers](https://forums.themanaworld.org/memberlist.php?mode=group&g=979)
  - [Game Masters](https://forums.themanaworld.org/memberlist.php?mode=group&g=973)
- [Manasource Advisers](https://forums.themanaworld.org/memberlist.php?mode=group&g=984)
  - [Advisers](https://forums.themanaworld.org/memberlist.php?mode=group&g=984)

Note: You must be registered on Forums to see the member list.
